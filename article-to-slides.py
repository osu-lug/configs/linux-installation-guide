#!/usr/bin/env python3
import re
import sys
slide_started=0
last_header=["","",""]#\section,\subsection\subsubsection
def begin_slide(slide_started,title,next_line,force_slide=False):
    if not "section{" in next_line or force_slide: # make sure we don't have any empty slides.
        if slide_started == 1:
            print("\\end{frame}")
        print("\\begin{frame}{"+title+"}")
        #print("\\begin{slide}")
        #print("\\LARGE{"+re.search(r'\{.*\}',line).group(0)[1:-1]+"}\par\\large")
def get_content_from_line(line):
    return re.search(r'\{.*\}',line).group(0)[1:-1]
def get_header(last_header):
    output=last_header[0]
    for i in range(1,len(last_header)):
        if last_header[i]!=None:
            output+=" - "+last_header[i]
    return output
f_next=open(sys.argv[1],'r')
f_next.readline()
with open(sys.argv[1],'r') as f:
    for line in f:
        next_line=f_next.readline()
        if "\\documentclass" in line:
            bracket="landscape"
            if re.search(r'\[.+\]',line):
                bracket+=","+re.search(r'\[.+\]',line).group(0)[1:-1]
#            print("\\documentclass["+bracket+"]{slides}")
            print("\\documentclass["+bracket+"]{beamer}")
            #print("\\usepackage[landscape]{geometry}")
        elif "\\tableofcontents" in line:
            print("\\maketitle")
        elif "\\rhead" in line:
            ignore=0
        elif "\\lhead" in line:
            ignore=0
        elif "\\rfoot" in line:
            ignore=0
        elif "\\lfoot" in line:
            ignore=0
        elif "\\usepackage{fancyhdr}" in line:
            ignore=0
        elif "\\fancyhf" in line:
            ignore=0
        elif "\\newpage" in line:
            ignore=0
        elif "\\newline" in line:
            ignore=0
        elif "\\pagestyle" in line:
            ignore=0
        elif "\\end{document}" in line:
            print("\\end{frame}")
            print(line)
        elif "\\section" in line:
            last_header[0]=get_content_from_line(line)
            last_header[1]=None
            last_header[2]=None
            begin_slide(slide_started,get_header(last_header),next_line)
            slide_started=1
            #print("% section FOUND")
        elif "\\subsection" in line:
            last_header[1]=get_content_from_line(line)
            last_header[2]=None
            begin_slide(slide_started,get_header(last_header),next_line)
            #print("% subsection FOUND")
        elif "\\subsubsection" in line:
            last_header[2]=get_content_from_line(line)
            begin_slide(slide_started,get_header(last_header),next_line)
            #print("% subsubsection FOUND")
        elif "\\includegraphics" in line:
            begin_slide(slide_started,get_header(last_header),next_line,True)
            if "[" in line:
                print(re.sub(r'(.*?)\\includegraphics\[(.*?)\](.*?)',r'\1\\includegraphics[width=\\linewidth]\3',line),end='')
            else:
                print(line,end='')
        elif "\\paragraph" in line:
            print("\\Large{"+get_content_from_line(line)+"}")
        #elif "\\verb" in line:
        #    print(re.sub(r'(.*?)\\verb\|(.*?)\|(.*?)',r'\1"\2"\3',line))
        else:
            print(line,end='')
print()
f_next.close()
